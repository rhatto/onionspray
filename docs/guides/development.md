# Development

Onionspray development guidelines and workflow are listed here.

## Release procedure

Release cycle workflow.

### Version update

Set the version number:

    ONIONSPRAY_VERSION=1.6.2

Update the version in some files, like:

    $EDITOR onionspray

### Register the changes

Update the ChangeLog:

    $EDITOR docs/changelog.md

Commit and tag:

    git diff # review
    git commit -a -m "Feat: Onionspray $ONIONSPRAY_VERSION"
    git tag -s v${ONIONSPRAY_VERSION} -m "Onionspray $ONIONSPRAY_VERSION"

Push changes and tags. Example:

    git push origin        && git push upstream
    git push origin --tags && git push upstream --tags

### Announcement

Announce the new release:

* Post a message to the [Tor Forum][], using the [onion-services-announce tag][].
* Send a message to the [tor-announce][] mailing list ONLY in special cases,
  like important security issues (severity `HIGH` or `CRITICAL`).

Template:

```
Subject: [RELEASE] Onionspray [security] release $ONIONSPRAY_VERSION

Greetings,

We just released [Onionspray][] $ONIONSPRAY_VERSION, an application to serve
regular websites as onionsites.

[This release fixes a security issue. Please upgrade as soon as possible!]

[Onionspray]: https://onionservices.torproject.org/apps/web/onionspray

# ChangeLog

$CHANGELOG
```

[tor-announce]: https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-announce
[Tor Forum]: https://forum.torproject.org
[onion-services-announce tag]: https://forum.torproject.org/tag/onion-services-announce

## Upstream monitoring

If you're developing Onionspray, it's recommended to to get alerts on
important upstream announcements:

* For updates on C Tor, follow the [tor-announce][] mailing list.
* Subscribe to the [OpenResty GitHub repository][openresty] to get
  notifications upon releases and security alerts.

[tor-announce]: https://lists.torproject.org/mailman3/postorius/lists/tor-announce.lists.torproject.org/
[openresty]: https://github.com/openresty/openresty
