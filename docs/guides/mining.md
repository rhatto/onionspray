# Tips when mining Onion Addresses

You are setting up an Onion site! And you want a [vanity .onion address][]!
There is lots of software out there that you can use to generate them!

[vanity .onion address]: https://community.torproject.org/onion-services/advanced/vanity-addresses/

## What to use?

Some people mine .onion addresses on local hardware for privacy and safety,
whilst others are happy to rent a GPU-based compute-heavy instance from AWS, or
similar.

We're not going to make terribly strong software recommendations, because it's a
matter of what you have at your disposal already, and what fits the hardware
that you have access to.

For instance, you can build a small cluster of Raspberry Pi, and use
[mkp224o][] for mining with them, by compiling the code using [the
instructions][] and [the tuning instructions][] to build something suitable for
this use-case. You can also give [Onionmine][] a try.

If you are looking for a really meaningful, long-prefix onion address up front,
you will have to expend a lot of money and CPU-time in order to find one. This
is why the next section is really important in order to get the most "bang for
your buck".

Remember that this process is also energy-intensive, so we don't recommend that
you go after a very long prefix. Try to get something up 7 characters.

[mkp224o]: https://github.com/cathugger/mkp224o
[the instructions]: https://github.com/cathugger/mkp224o/blob/master/README.md
[the tuning instructions]: https://github.com/cathugger/mkp224o/blob/master/OPTIMISATION.txt
[Onionmine]: https://gitlab.torproject.org/tpo/onion-services/onionmine/

## How to best approach this challenge?

If you're setting up multiple onions for your site -- eg. if there is one/more
CDNs associated with your site, it is nice to set up vanity onions for them,
too; partly for "cute" but also to stop yourself going crazy during debugging.

For instance, the (defunct, v2) NYT onion was
https://www.nytimes3xbfgragh.onion/ and their CDN onion was
https://graylady3jvrrxbe.onion/

Similarly there were https://www.facebookcorewwwi.onion/ and
https://fbcdn23dssr3jqnq.onion/ for Facebook.

Ask yourself now: perhaps use your CDN Onion to reflect your own history and
site/brand culture? Perhaps you can mine several onion addresses at the same
time, even speculatively?

Onion mining is a matter of luck and expensive resource, and
(counterintuitively?) the rarest resource that you have, is time as-measured by
your wall clock.

Therefore, if you are mining onions for a lot of sites, the best strategy is
follows:

* Have breakfast and some tea or coffee. Try to get into a creative mood.  You
  are making an investment of time and energy *now* to save yourself time and
  effort, later.

* Sit down, open a document, and try to think inclusively of EVERY POSSIBLE
  PREFIX THAT YOU MIGHT EVER FIND ACCEPTABLE at the start (or suffix, at the
  end) of your onion addresses, for all of your sites and CDNs, and write them
  all down. You may create 10, 20, or more. No ideas are bad ideas. Deduplicate
  them (e.g.: it's pointless to look for `nytimes` if you're already looking for
  anything beginning with `nyt`).  Each additional prefix is nearly zero-cost,
  compared to the days, weeks, or months of time that your computers will spend
  in grinding their way through cryptography.

* Configure your software to search for all of these, for all of your sites,
  simultaneously. Set it running. Make sure to configure options (or: wrap it
  in a shell script) so that it runs 24x7, saving all the successful matches into
  the local filestore.

* If/when you think of yet another prefix, stop your software, configure the
  extra prefix, and start it running again. Save all of the successful matches,
  never throw anything away.

* When you are approaching ship-date, get all the relevant parties together (or
  just yourself) and grab some beer/wine and use `grep` to go looking for the
  best ones. Eyeball the whole list, if you can.

* You will be surprised -- especially if you've invested fully into choosing as
  many meaningful prefixes as possible -- because you're dealing with
  randomness here, and raw entropy is more creative than you'd ever imagine.

* There is also a vast amount of noise -- huge, enormous quantities of
  gibberish -- but that's okay, because (again) `storage+grep` is much cheaper
  than `encryption+wallclocktime`.

* When we mined the Facebook onion address, the search-patterns were
  `^(facebook|fbcdn|fbsbx|...)` and a few others all in a single pattern.  We
  drank beer and spent a few days deciding amongst the good ones.

* Similarly the pattern for the NY Times was
  `^(nytimes|nytcdn|nytwww|graylady|...)` and a few other potential prefixes,
  perhaps a dozen, all in one pattern; and I mined onion addresses for other
  sites at the same time, on the same hardware, in the same process.

* Why do it this way? In short, because encryption is relatively expensive, and
  string comparisons are really cheap. Every single candidate onion address
  that you generate, should be tested against everything that you can imagine
  ever looking for, otherwise it's a wasted opportunity.

* Ideally, make sure that you are thoroughly in control of the backups and
  storage of the machine upon which you are doing the mining; try to use an
  encrypted partition if you can.

* Ensure that you have proper controls over all media which ever receives a
  copy of the Onion address key.

Best of luck to you. :-)

## Converting your v3 onion addresses for Onionspray

Tools like `mkp224o` save the keys they generate as three separate files:
`hs_ed25519_public_key`, `hs_ed25519_secret_key`, and `hostname`; this is
elegant but hard to manipulate, so Onionspray creates its own standard for
storing v3 onion addresses in the `secrets` folder.

If you are in a directory which contains the above-named three files, you can
run a helper shellscript by using a command, something like:

    ./onionspray/lib/rename-v3-keys-for-onionspray-secrets.sh

... which will safely create TWO files:

* `someverylongonionaddressinvolvingalotofbase32characterss.v3pub.key`
* `someverylongonionaddressinvolvingalotofbase32characterss.v3sec.key`

... that can be moved into your `~/onionspray/secrets/` folder, for Onionspray
to use when you run `onionspray config ...`

## Make sure to test the keys

Regardless of the tool you used to generate your keys, please test first with a
tor implementation before putting it on production, to ensure it will work
and is accepted by the tor daemon.

If you mine Onion Addresses, beware, and always test them thoroughly on
Onionspray, especially **before** buying TLS Certificates which cites them.

Check that it matches your expectation, and if not, discard that vanity address
and start over.
