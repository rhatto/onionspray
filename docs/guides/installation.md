# Installation

## Primary supported platforms

* Debian Bookworm
* Ubuntu 22.04 LTS

## Main requirements

Onionspray requires recent `tor` and also recent `openresty` with the
[ngx_http_substitutions_filter_module][] NGINX module
enabled[^filter-module-dependency].  They simply need to be accessible in the
`$PATH` environment variable.

The following "build" scripts simply supply that by building these software
from sources, but if you prefer you can use the ones provided by packages, or
build yourself.

This guide also assumes that you have familiarity with [Unix-like][] systems,
especialy when using a command line interface, and that you have access to
such systems.

[ngx_http_substitutions_filter_module]: https://github.com/yaoweibin/ngx_http_substitutions_filter_module
[Unix-like]: https://en.wikipedia.org/wiki/Unix-like
[official OpenResty packages]: https://openresty.org/en/linux-packages.html
[relying only on the native OpenResty substitution filters]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/31

[^filter-module-dependency]: As of January 2024, the
  [ngx_http_substitutions_filter_module][] dependency means Onionspray won't
  work with the [official OpenResty packages][]. Newer Onionspray versions might
  come without this dependency and [fully support upstream packages][], [relying
  only on the native OpenResty substitution filters][].

[fully support upstream packages]: https://tpo.pages.torproject.net/onion-services/onionspray/guides/installation/

## Optional requirements

Onionspray have optional requirements, depending on what you want to do:

* [Onionbalance][] needs to installed and available in your `$PATH`
  environmente variable if you want to use [softmaps](balance/softmaps.md).
  It may be already available as a package in your distribution and be
  automatically installed by some of the provided build/installation
  scripts below, so we recommend you to check that after the installation
  procedure.

[Onionbalance]: https://onionservices.torproject.org/apps/base/onionbalance/

## Preparation

You main want to add the `onionspray` folder into your `$PATH`, so you can
invoke Onionspray anywhere:

    export PATH=$PATH:/path/to/onionspray

This way, you can invoke `onionspray` by simply typing

    onionspray

Instead of

    ./onionspray

Or instead of invoking by it's full path name, such as

    /path/tonionspray/onionspray

## Per-platform installation

Onionspray is installed directly from it's [repository][].

!!! note "Building from sources"

    All installations are currently being built from fresh sources for reasons of
    development and keeping up to date with core, evolving features. As such,
    installations make take extensive time to build, especially on slower machines.

    Check issue [tpo/onion-services/onionspray#33][] for details.

!!! tip "Keeping the source code"

    By default, the Onionspray build procedures will cleanup all dependencies'
    source code after compilation.

    Use the `--keep-sources` flag on the build scripts to preserve the sources,
    which can be helpful for debugging, testing and other tinkering. Builds
    running a second time with this flag tends to complete faster.

[tpo/onion-services/onionspray#33]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/33

[repository]: https://gitlab.torproject.org/tpo/onion-services/onionspray/

### Debian 12 (bookworm)

<!-- TODO: This needs a macro? -->
In a Debian bookworm instance:

    sudo apt update
    sudo apt install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-debian-bookworm.sh

<!-- TODO: This needs a macro? -->
There's also a [install-debian-bookworm.sh][] script which uses Tor and
OpenResty upstream packages, but this procedure is not entirelly supported
right now due to a dependency on an externa NGINX
module[^filter-module-dependency].

[install-debian-bookworm.sh]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/blob/main/opt/install-debian-bookworm.sh

### Debian 11 (bullseye)

In a Debian bullseye instance:

    sudo apt update
    sudo apt install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-debian-bookworm.sh

### Ubuntu 24.04LTS (noble)

Install a `ubuntu-24.04.2-live-server-amd64.iso` server instance; and then:

    sudo apt update
    sudo apt-get install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-ubuntu-noble.sh

### Ubuntu 22.04LTS (jammy)

Install a `ubuntu-22.04.2-live-server-amd64.iso` server instance; and then:

    sudo apt update
    sudo apt-get install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-ubuntu-jammy.sh

<!-- TODO: This needs a macro? -->
There's also a [install-ubuntu-22.04.sh][] script which uses Tor and
OpenResty upstream packages, but this procedure is not entirelly supported
right now due to a dependency on an externa NGINX
module[^filter-module-dependency].

[install-ubuntu-22.04.sh]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/blob/main/opt/install-ubuntu-22.04.sh

### Ubuntu 20.04LTS (focal)

Install a `ubuntu-20.04.2-live-server-amd64.iso` server instance; and then:

    sudo apt update
    sudo apt-get install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-ubuntu-focal.sh

### Ubuntu 18.04LTS (bionic)

Install a `ubuntu-18.04.2-live-server-amd64.iso` server instance; and then:

    sudo apt update
    sudo apt-get install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-ubuntu-bionic.sh

### Raspbian 9 (stretch)

    sudo apt-get install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-raspbian-stretch.sh

### CentOS 8.2.2004

Install a minimal server and then:

    sudo yum -y install git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-centos-8.2.2004.sh

### FreeBSD 12.1

Install a base server and then:

    pkg install git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-freebsd-12.1.sh

### macOS 10.15 (Catalina)

Install [Homebrew](https://brew.sh); and then:

    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-macos-catalina.sh

### macOS 10.14 (Mojave)

Install [Homebrew](https://brew.sh); and then:

    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-macos-mojave.sh

## Notes
