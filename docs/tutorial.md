# Quick Start Tutorial

## Setp 0: requirements

To setup Onionspray, you'll need to:

* Have access to a [Unix-like][] system such as Debian with internet access and
  able to reach the Tor network.
* Have a basic knowledge to use a [Unix-like][] command line interface (a
  "terminal", or "shell access").

[Unix-like]: https://en.wikipedia.org/wiki/Unix-like

## Step 1: install Onionspray and build the necessary executables

<!-- TODO: This needs a macro? -->
Example using Debian bookworm:

    sudo apt update
    sudo apt install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-debian-bookworm.sh

Installation procedures for other systems can be found at the [installation
guide](guides/installation.md).

## Step 2: create a template config file

Create a file named `myproject.tconf` that contains:

    # boilerplate, adjust if needed
    set nginx_resolver 8.8.8.8 1.1.1.1 ipv6=off
    set nginx_cache_seconds 60
    set nginx_cache_size 64m
    set nginx_tmpfile_size 8m
    set log_separate 1

    # make sure to test upstream HTTPS certificates
    #
    # you might need to adjust this setting depending on the Operating System
    # you run; and you may even need to install and maintain this file yourself
    # somewhere
    set nginx_proxy_ssl_trusted_certificate /etc/ssl/certs/ca-certificates.crt

    # preserve this domain name in free text or email addresses
    set preserve_csv save,mydomain\\.com,i,mydomain.com

    set project myproject
    hardmap %NEW_V3_ONION% mydomain.com

... and amend the values of `mydomain` and `com` (if you are not using a `.com`
top level domain) throughout that file.

## Step 3: generate an actual configuration

Run:

    ./onionspray config myproject.tconf

... which will create `myproject.conf` and populate it with onion addresses.

## Step 4: generate startup scripts

Run:

    ./onionspray script

... which will generate startup and housekeeping scripts; read them for
installation instructions.

## Step 5: start your server

Run:

    ./onionspray start myproject

... to start your server; then do:

    ./onionspray maps myproject

... to see what onion address to connect to.

## References

Other (possibly outdated, or written for [EOTK][]) installation references:

* [Make Your Site Available Over Tor: Guide To EOTK, The Enterprise Onion Toolkit](https://shen.hong.io/making-websites-on-tor-using-eotk/)

[EOTK]: https://github.com/alecmuffett/eotk
