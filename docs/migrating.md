# EOTK to Onionspray migration tutorial

Onion Service Operators wanting to move from [EOTK][] to Onionspray may need to
do a few steps to start using code from the new repository.

This tutorial is a reference in how you can migrate your setup.

!!! tip

    When migrating, do not reuse your existing [EOTK][] installation.
    Instead, install Onionspray in another folder and copy over your
    configuration files and keys.

!!! info

    If you run into any problem happens when migrating, you can safely switch
    to the old EOTK repository to gain time to troubleshoot.

!!! note

    Report problems at the [Onionspray issue queue][].

[EOTK]: https://github.com/alecmuffett/eotk
[Onionspray issue queue]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues

## Recommended migration procedure

### 1. Install Onionspray

Example using Debian bookworm:

    sudo apt update
    sudo apt install -y git
    git clone https://gitlab.torproject.org/tpo/onion-services/onionspray.git
    cd onionspray
    ./opt/build-debian-bookworm.sh

Installation procedures for other systems can be found at the [installation
guide](guides/installation.md).

### 2. Copy your configuration files

Assuming you're inside the Onionspray folder, run this command:

    cp /path/to/eotk/*.*conf .

Then rename:

* `eotk-site.conf` to `onionspray-site.conf`.
* `eotk-workers.conf` to `onionspray-workers.conf`.

### 3. Copy your Onion Service keys

Now copy the Onion Service keys:

    cp -r /path/to/eotk/secrets.d secrets

### 4. Reconfigure your projects

Run this command for each project (say `myproject.conf`) to configure it:

    ./onionspray config myproject.conf

### 5. Copy your HTTPS certificates

Run this command for each project (say `myproject`) to copy your existing
HTTPS certificates:

    cp /path/to/eotk/projects.d/myproject.d/ssl.d/* projects/myproject/ssl/

### 7. Copy old logs (optional)

Optionally copy old log files from each project:

    cp /path/to/eotk/projects.d/myproject.d/log.d/* projects/myproject/log/

### 6. Start Onionspray

Now it's time to start Onionspray:

    ./onionspray bounce -a

If you prefer, you can stop EOTK before doing this (see the next step).

### 7. Stop EOTK

You can keep both EOTK and Onionspray running in parallel for a while to try a
seamless migration (due to [load balancing
properties](guides/balance/README.md)), or jumping straight away to stop EOTK
by running this command:

    /path/to/eotk/eotk stop -a

### 8. Replacing cronjobs and initialization scripts

Don't forget to update your initialization scripts and cronjobs to use the
new name, folder and `onionspray` script.

You can use the `onionspray` command to generate new scripts:

    ./onionspray make-scripts

And then replace the EOTK boot and cron scripts with the newer ones.

<!--
## Non-recommended migration procedure

!!! warning

    This procedure is not recommended, as it reuses an existing EOTK
    installation, and may be harder to do, or break things.

### 0. Backup

Backup your EOTK folder.

### 1. Copy EOTK as Onionspray

Copy the `eotk` folder into `onionspray`.

### 2. Update the codebase and config file names

In the new `onsionspray` folder, update the repository remotes to the new
location:

    git remote set-url origin https://gitlab.torproject.org/tpo/onion-services/onionspray.git

Then rename:

* `eotk-site.conf` to `onionspray-site.conf`.
* `eotk-workers.conf` to `onionspray-workers.conf`.

Also make sure to remove the `.d` suffix for folders such as `projects`,
`secrets` and `onionbalance.d`, and all it's subfolders. A `find -type d -name
'*.d'` may be helpful.

And don't forget to update your initialization scripts and cronjobs to use the
new name, folder and `onionspray` script.

### 3. Switch to the new service

* Stop all projects in the old EOTK instance.

* Start all projects in the new Onionspray instance.

* Test the setup, as the fork/rename may introduce some minor issues.
-->
